package com.laksh;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FunctionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void postFunction() throws Exception {
        ResultMatcher ok = MockMvcResultMatchers.status().isOk();
        String requestBody = "Writing into a file";
        mvc.perform(MockMvcRequestBuilders.post("/function").accept(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(ok);
        String content = new String(Files.readAllBytes(Paths.get("static/function-app.py")), Charset.forName("UTF-8"));
        MatcherAssert.assertThat(requestBody, Matchers.equalTo(content));
    }
}
