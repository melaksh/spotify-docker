package com.laksh;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;

public class FunctionService {

    public void createFunction(String fn) {
        writeUsingFiles(fn);
        // executeFile();
    }

    private void writeUsingFiles(String data) {
        Path filePath = Paths.get("static/function-app.py");
        try {
            Files.write(filePath, data.getBytes());
            Files.setPosixFilePermissions(filePath, PosixFilePermissions.fromString("rwxrwxrwx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeFile() {
        try {
            DockerUtils docker = new DockerUtils();
            String imageId = docker.createImage();
            docker.createContainer(imageId);
        } catch (Exception e) {
            System.err.println("ERROR EXECUTING FUNCTION - " + e.getMessage());
            e.printStackTrace();
        }
    }
}
