package com.laksh;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FunctionController {

    @RequestMapping(value = "/function", method = RequestMethod.POST)
    public ResponseEntity<String> function(@RequestBody String function) {
        System.out.println(function);
        if (function == null || function.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        }
        FunctionService service = new FunctionService();
        service.createFunction(function);
        // executeFile();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
