package com.laksh;

import static com.spotify.docker.client.DockerClient.ListContainersParam.allContainers;
import static com.spotify.docker.client.DockerClient.ListImagesParam.allImages;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.ProgressHandler;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.Image;
import com.spotify.docker.client.messages.PortBinding;
import com.spotify.docker.client.messages.ProgressMessage;

/**
 * Hello world!
 */
public final class AppSpotify {

    private static DockerClient docker = null;

    private AppSpotify() {
    }

    /**
     * Says hello to the world.
     *
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        System.out.println("Docker using Spotify Client!");
        try {
            docker = DefaultDockerClient.fromEnv().build();
            listContainers();
            listImages();
            String id = buildImage();
            buildContainer(id);
            listImages();
        } catch (Exception e) {
            System.out.println("Got an Exception :-(");
            e.printStackTrace();
        }

        System.out.println("We are done :-)");
    }

    private static void listContainers() throws Exception {
        System.out.println("Listing all containers");
        final List<Container> containers = docker.listContainers(allContainers());
        containers.stream().forEach(c -> System.out.println(c.names().get(0)));
        System.out.println("...");
    }

    private static void listImages() throws Exception {
        System.out.println("Listing all Images");
        final List<Image> images = docker.listImages(allImages());
        images.stream().forEach(i -> System.out.println(i.id()));
        System.out.println("...");
    }

    private static String buildImage() throws Exception {
        System.out.println("Building an image");
        final AtomicReference<String> imageIdFromMessage = new AtomicReference<>();
        String dockerDirectory = "/Users/ls043274/MySpace/Docker/spotify-docker";
        final String returnedImageId = docker.build(Paths.get(dockerDirectory), "python-test-app",
                new ProgressHandler() {
                    @Override
                    public void progress(ProgressMessage message) throws DockerException {
                        final String imageId = message.buildImageId();
                        if (imageId != null) {
                            imageIdFromMessage.set(imageId);
                        }
                    }
                });
        System.out.println("Image id - " + returnedImageId);
        return returnedImageId;
    }

    private static String buildContainer(String imageId) throws Exception {
        // imageId = "2e19b4a59b60";
        System.out.println("Building a container");
        final String[] ports = { "80" };
        final Map<String, List<PortBinding>> portBindings = new HashMap<>();
        Arrays.stream(ports).forEach(port -> {
            List<PortBinding> hostPorts = new ArrayList<>();
            hostPorts.add(PortBinding.of("0.0.0.0", port));
            portBindings.put(port, hostPorts);
        });

        final HostConfig hostConfig = HostConfig.builder().portBindings(portBindings).build();
        final ContainerConfig containerConfig = ContainerConfig.builder().hostConfig(hostConfig)
                .image("python-test-app").exposedPorts(ports).build();
        final ContainerCreation container = docker.createContainer(containerConfig);
        docker.startContainer(container.id());

        final ContainerInfo info = docker.inspectContainer(container.id());
        System.out.println(info.name() + " - " + info.state().toString());
        return container.id();
    }

}
